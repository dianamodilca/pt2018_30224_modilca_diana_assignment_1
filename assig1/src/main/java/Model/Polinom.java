package Model;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
	

	public ArrayList<Monom> monom;
	
	public Polinom () {
		monom = new ArrayList<Monom>();
	}
	
	
	public ArrayList<Monom> getMonom() {
		return monom;
	}


	public void setMonom(ArrayList<Monom> monom) {
		this.monom = monom;
	}


	public static Polinom convertToPolinom (String s) {
		Pattern pattern = Pattern.compile("([+-]?(?:(?:\\d+x\\^\\d+)|(?:\\d+x)|(?:\\d+)|(?:x)))");
		Matcher matcher = pattern.matcher(s);
		Polinom p = new Polinom();
		while (matcher.find()) {
		    String str = matcher.group(1);

		    if (str.matches(".*\\^.*")) {
	      
	            String[] split = str.split("\\^");
	         
	            String exponent = split[1];
	            int exp= Integer.parseInt(exponent);
				String[] parts = str.split("x");
				String part1 = parts[0]; 
			
				double coeficient = Double.parseDouble(part1);
				Monom m = new Monom();
				m.setCoeff(coeficient);
				m.setExp(exp);
				p.monom.add(m);
				
			}
		}
		return p;
	}
	
	public String convertToString(Polinom p) {
		String s="";
		for(Monom i: p.monom) {
			
			if(i.getCoeff()>=0)
			s+="+";
			s = s + String.valueOf(i.getCoeff()) + "x^" + String.valueOf(i.getExp());
		}
		return s;
		
	}
	
	public Polinom(String s) {
		
	}
	
	public Polinom eliminaDuplicate(Polinom p) {

        for (Monom i: p.monom) {
            for (Monom j: p.monom) {
                if (i.getExp() == j.getExp()) {
                    i.setCoeff(i.getCoeff() + j.getCoeff());
                  
                }
            }
       }
        
        return p;
    }

}

