package Model;

public class Monom {

	private double coeff;
	private int exp;
		
	public Monom(double coeff, int exp) 
	{
		this.coeff = coeff;
		this.exp = exp;
	}
	
	public void setCoeff(double d) {
		this.coeff = d;
	}

	public void setExp(int d) {
		this.exp = d;
	}

	public double getCoeff() {
		return this.coeff;
	}


	public int getExp() {
		return this.exp;
	}

	public Monom() {
		
	}
	
	public int compareTo(Monom m) {
		int compare = ((Monom) m).getExp();
		return compare - this.getExp();

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monom other = (Monom) obj;
		if (Double.doubleToLongBits(coeff) != Double.doubleToLongBits(other.coeff))
			return false;
		if (exp != other.exp)
			return false;
		return true;
	}


}