package GUI;


import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import Model.Polinom;


public class View { 
	private JFrame frame = new JFrame();
	public JTextField txtFieldp1;
	public JTextField txtFieldp2;
	public JTextField txtFieldrez;
	private JLabel labelp1;
	private JLabel labelp2;
	private JLabel labelResult;
	private JButton buttonAdd;
	private JButton buttonSubstract;
	private JButton buttonMul;
	private JButton buttonDivide;
	private JButton buttonDeriv;
	private JButton buttonIntegr;
	private JPanel panel = new JPanel();
	
	Polinom p1;
	Polinom p2;
	
	public View() {
		
		frame.setVisible(true);
		frame.setTitle("Polynomial");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(500, 500); 
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		panel.setVisible(true);
		panel.setLayout(null);
		panel.setSize(500, 500); 
		frame.add(panel);
		
		
		labelp1 = new JLabel("Polinom 1: ");
		panel.add(labelp1);
		labelp1.setSize(180, 180); 
		labelp1.setLocation(15,25);
		
		txtFieldp1 = new JTextField();
		panel.add(txtFieldp1);
		txtFieldp1.setSize(150, 25);
		txtFieldp1.setLocation(150,105);
	
				
		labelp2 = new JLabel("Polinom 2: ");
		panel.add(labelp2);
		labelp2.setSize(180, 180); 
		labelp2.setLocation(15,75);

		
		txtFieldp2 = new JTextField(10);
		panel.add(txtFieldp2);
		txtFieldp2.setSize(150, 25);
		txtFieldp2.setLocation(150,150);
	
		
		buttonAdd = new JButton("ADD");
		panel.add(buttonAdd);
		buttonAdd.setSize(120,50);
		buttonAdd.setLocation(10,200);
		
		buttonSubstract = new JButton("SUBSTRACT");
		panel.add( buttonSubstract);
		buttonSubstract.setSize(120,50);
		buttonSubstract.setLocation(150,200);
		
		buttonMul = new JButton("MULTIPLY");
		panel.add(buttonMul);
		buttonMul.setSize(120,50);
		buttonMul.setLocation(290,200);
		
		buttonDivide = new JButton("DIVIDE");
		panel.add(buttonDivide);
		buttonDivide.setSize(120,50);
		buttonDivide.setLocation(10,270);
		
		buttonDeriv = new JButton("DERIVATE");
		panel.add(buttonDeriv);
		buttonDeriv.setSize(120,50);
		buttonDeriv.setLocation(150,270);
		
		buttonIntegr = new JButton("INTEGRATE");
		panel.add(buttonIntegr);
		buttonIntegr.setSize(120,50);
		buttonIntegr.setLocation(290,270);
		
		labelResult = new JLabel("Result: ");
		panel.add(labelResult);
		labelResult.setSize(180, 180); 
		labelResult.setLocation(15,273);
		
		txtFieldrez = new JTextField(20);
		txtFieldrez.setEditable(false);
		panel.add(txtFieldrez);
		txtFieldrez.setSize(250, 60);
		txtFieldrez.setLocation(150,350);
		
	}
		
	public void actionAdd(ActionListener e) {
		this.buttonAdd.addActionListener(e);
	}

	public void actionSub(ActionListener e) {
		this.buttonSubstract.addActionListener(e);
	}

	public void actionMul(ActionListener e) {
		this.buttonMul.addActionListener(e);
	}

	public void actionDiv(ActionListener e) {
		this.buttonDivide.addActionListener(e);
	}

	public void actionDeriv(ActionListener e) {
		this.buttonDeriv.addActionListener(e);
	}

	public void actionIntegr(ActionListener e) {
		this.buttonIntegr.addActionListener(e);
	}
	

	public String getTextp1() {
		return txtFieldp1.getText();
	}

	public void setTextp1(String txtFieldp1) {
		this.txtFieldp1.setText(txtFieldp1);
	}

	public String getTextp2() {
		return txtFieldp2.getText();
	}

	public void setTextp2(String txtFieldp2) {
		this.txtFieldp2.setText(txtFieldp2);
	}
	
	public JTextField getTextrez() {
		return txtFieldrez;
	}

	public void setTextrez(String txtFieldrez) {
		this.txtFieldrez.setText(txtFieldrez);
	}


	
}

