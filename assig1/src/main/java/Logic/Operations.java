package Logic;

import java.util.Collections;
import java.util.Comparator;

import GUI.View;
import Model.Monom;
import Model.Polinom;



public class Operations extends View {
	
	Polinom pol1 = new Polinom ();
	Polinom pol2 = new Polinom ();
	Polinom result = new Polinom ();
	
	public void ordTerms() {
		Comparator<Monom> exp = new Comparator<Monom>() {

			@Override
			public int compare(Monom arg0, Monom arg1) {
				return arg1.getExp() - arg0.getExp();
			}

		};

		//Collections.sort(this.getMonom(), exp);

	}
	
	public Polinom adunare(Polinom pol1, Polinom pol2) {
		
		Polinom result = new Polinom ();
		
		for(Monom m: pol1.getMonom())
			for(Monom n: pol2.getMonom())
			{
				if(m.getExp() > n.getExp())
				{
					result.monom.add(m);
					break;
				}
				
				if(m.getExp() == n.getExp())
				{
					Monom mon = new Monom(m.getCoeff() + n.getCoeff(), m.getExp());
					result.monom.add(mon);
					break;
				}	
			
			}
	
		return result;
	}
	
	public Polinom sub(Polinom pol1, Polinom pol2) {
		
		Polinom result = new Polinom ();
		
		for(Monom m: pol1.getMonom())
			for(Monom n: pol2.getMonom())
			{
				if(m.getExp() > n.getExp())
				{
					result.monom.add(m);
					break;
				}
				
				if(m.getExp() == n.getExp())
				{
					Monom mon = new Monom(m.getCoeff() - n.getCoeff(), m.getExp());
					result.monom.add(mon);
					break;
				}	
			
			}
	
		return result;

	}
	
	public Polinom mul(Polinom pol1, Polinom pol2) {
		
		Polinom result = new Polinom();
		
		for(Monom m: pol1.monom) {
			for(Monom n: pol2.monom) {
				Monom mon = new Monom(m.getCoeff()*n.getCoeff(), m.getExp()+n.getExp());
				result.monom.add(mon);
			}
		}
		
		result = eliminaDuplicate(result);
		
		return result;

	}

	private Polinom eliminaDuplicate(Polinom p) {
		for (Monom i: p.monom) {
            for (Monom j: p.monom) {
                if (i.getExp() == j.getExp()) {
                    i.setCoeff(i.getCoeff() + j.getCoeff());
                  
                }
            }
		}
		return p;
	}

	public Polinom deriv(Polinom pol1) {
		
		Polinom result = new Polinom();
		
		for(Monom m: pol1.monom) {
			if(m.getExp() != 0)	{
				Monom mon = new Monom(m.getExp()*m.getCoeff(), m.getExp()-1);
				result.monom.add(mon);
			}
		}
		
		return result;
	}

	public Polinom integr(Polinom pol1) {

		Polinom result = new Polinom();
		
		for(Monom m:pol1.monom) {
			Monom mon = new Monom(m.getCoeff()/(m.getExp()+1), m.getExp()+1);
			result.monom.add(mon);
		}
		return result;

	}
	
}
	

