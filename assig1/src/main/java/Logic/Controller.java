package Logic;


import javax.swing.JOptionPane;

import GUI.View;

import Model.Polinom;


public class Controller {
	
	Polinom pol1 = new Polinom();
	Polinom pol2 = new Polinom();
	Polinom result = new Polinom();

	View view1 = new View();

	public Controller() {
				
		
		view1.actionAdd(e -> {
			
			Operations op = new Operations();
			view1.setTextrez("");
			
			try {
				pol1 = Polinom.convertToPolinom(view1.getTextp1());
				pol2 = Polinom.convertToPolinom(view1.getTextp2());
				result = op.adunare(pol1, pol2);
				view1.setTextrez(result.convertToString(result));
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Please input some data");
			}
			
		});
		
		view1.actionSub(e -> {
			
			Operations op = new Operations();
			view1.setTextrez("");
			
			try {
				pol1 = Polinom.convertToPolinom(view1.getTextp1());
				pol2 = Polinom.convertToPolinom(view1.getTextp2());
				result = op.sub(pol1, pol2);
				view1.setTextrez(result.convertToString(result));
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Please input some data");
			}
			
		});
		
		view1.actionMul(e -> {
			
			Operations op = new Operations();
			view1.setTextrez("");
			
			try {
				pol1 = Polinom.convertToPolinom(view1.getTextp1());
				pol2 = Polinom.convertToPolinom(view1.getTextp2());
				result = op.mul(pol1, pol2);
				view1.setTextrez(result.convertToString(result));
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Please input some data");
			}
			
		});
		
		view1.actionDiv(e -> {
			
			view1.setTextrez("");
			
			System.out.println("Operatia de impartire nu a fost implementata!");

		});
		
		view1.actionDeriv(e -> {
			
			Operations op = new Operations();
			view1.setTextrez("");
		
			try {
				pol1 = Polinom.convertToPolinom(view1.getTextp1());
				result = op.deriv(pol1);
				view1.setTextrez(result.convertToString(result));
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Please input some data");
			}
			
			
		});
		
		view1.actionIntegr(e -> {
		
			Operations op = new Operations();
			view1.setTextrez("");
			
			try {
				pol1 = Polinom.convertToPolinom(view1.getTextp1());
				result = op.integr(pol1);
				view1.setTextrez(result.convertToString(result));
			}catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Please input some data");
			}
			
		});

	}

}

